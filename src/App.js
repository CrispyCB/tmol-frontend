import './App.css';
import logo from './logo.svg'
import { useEffect } from 'react';
import DateComponent from './components/Date/DateComponent';
import ProgressBar from './components/ProgressBar/ProgressBar';
import NewItemBar from './components/NewItemBar/NewItemBar';
import TodoList from './components/TodoList/TodoList';
import CompletedList from './components/CompletedList/CompletedList';
import todoState from './todoStore';


function App() {
  const todos = todoState(state => state.todos);
  const getTodos = todoState(state => state.getTodos)
  useEffect(() => {
    getTodos();
  })
  return (
    <div id='tmol-frontend'>
      <img id='tmol-logo' src={logo} />
      <div id='tmol-content'>
        <section id='date-progress-wrapper'>
          <DateComponent />
          <ProgressBar progress={todos.filter(todo => todo.done === true).length} total={todos.length}/>
        </section>
        <NewItemBar />
        <TodoList />
        <CompletedList />
      </div>
    </div>
  );
}

export default App;
