import './Todo.css'
import { useCountdown } from '../../../hooks/useCountdown';
import DoneIcon from '@mui/icons-material/Done';
import ClearIcon from '@mui/icons-material/Clear';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import ky from 'ky';
import todoState from '../../../todoStore';

function Todo(props) {
    const {todo, created_at, id } = props.todo
    const [minutes, seconds] = useCountdown(created_at)
    const { completeTodo, deleteTodo } = todoState();
    if (minutes <= 0 && seconds <= 0) {
        return null;
}
else {
    return (
        <article className='todo'>
            <p>{todo}</p>
            <p>{minutes < 10 ? '0' + minutes : minutes}:{seconds < 10 ? '0' + seconds : seconds}</p>
            <Box display="flex" alignItems="center">
            <IconButton onClick={() => {
                (async () => {
                    await ky.patch(`http://localhost:8000/todos/complete/${id}`)
                    completeTodo(id);
                })();
            }}>
            <DoneIcon sx={{color: '#87F9C5'}} />
            </IconButton>
            <IconButton>
            <ClearIcon sx={{color: '#FA9787'}} onClick={() => {
                ky.delete(`http://localhost:8000/todos/${id}`);
                deleteTodo(id)
            }}/>
            </IconButton>
            </Box>
        </article>
    )
}
};

export default Todo;