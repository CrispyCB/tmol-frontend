import './TodoList.css'
import Todo from './Todo/Todo'
import todoState from '../../todoStore'

function TodoList() {
    const todos = todoState(state => state.todos)
    return (
        <section id="todo-list">
          <p id="todo-list-header">To Do</p>
          {todos.filter(todo => todo.done === false).map(todo => {
            return (
            <Todo todo={todo}  key={todo.id}/>
                )}
          )}
        </section>
    )
}

export default TodoList;