import { useState } from 'react';
import ky from 'ky';
import './NewItemBar.css'
import todoState from '../../todoStore';

function NewItemBar() {
    const [item, setItem] = useState('');
    const { addTodo } = todoState();
    return (
        <section id='add-new-item-bar'>
          <input  id='add-new-item-input' type='text' placeholder='Add new item' onChange={(e) => setItem(e.target.value)}></input>
          <button id='add-item-button' onClick={(e) => {
            (async () => 
            { 
              const todo = await ky.post('http://localhost:8000/todos/', {json:{todo: item}}).json()
              addTodo(todo)
            })();
           
          }}>
            <span id='add-item-icon' className="material-icons">add</span>
          </button>
        </section>
    )
}

export default NewItemBar;