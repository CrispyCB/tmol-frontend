import todoState from '../../todoStore';
import './CompletedList.css'
import CompletedTodo from './CompletedTodo/CompletedTodo';

function CompletedList() {
    const todos = todoState(state => state.todos)
    return(
        <section id="completed-list">
          <p id="completed-list-header">Completed</p>
          {todos.filter(todo => todo.done === true).reverse().map(todo => {
            return (
                    <CompletedTodo todo={todo}  key={todo.id}/>
                )}
          )}
        </section>
    )
}

export default CompletedList;