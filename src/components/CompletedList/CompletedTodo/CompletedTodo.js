import './CompletedTodo.css'

function CompletedTodo(props) {
    const { todo } = props.todo
    return (
        <article className="completed-todo">
            <p className="completed-text">{todo}</p>
        </article>
    )
}

export default CompletedTodo;