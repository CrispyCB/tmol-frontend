import './DateComponent.css'

function DateComponent() {
    const monthOptions = {month: 'short'}
    const yearOptions = {year: 'numeric'}
    return (
        <section id='date'>
          <section id='date-block'>
            <article id='day-num'>{new Date().getDate()}</article>
            <section>
              <article id='month'>{new Date().toLocaleDateString('en-US', monthOptions)}</article>
              <article id='year'>{new Date().toLocaleDateString('en-US', yearOptions)}</article>
            </section>
            <article id='day-text'>Thursday</article>
          </section>
          </section>
    )
}

export default DateComponent;