import './ProgressBar.css';

function ProgressBar(props) {
    const percent = props.progress/props.total > 0 ? ((props.progress/props.total) * 100) + '%' : '0%'
    return (
        <section id='progress-bar'>
            <article id='progress' style={{width: percent}}></article>
          </section>
    )
}

export default ProgressBar;