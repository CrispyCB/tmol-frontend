import create from "zustand";
import ky from 'ky'

const todoState = create((set) => ({
    todos: [],
    getTodos: async () => {
        const fetchedTodos = await ky.get('http://localhost:8000/todos').json();
        set({ todos: fetchedTodos })
    },
    addTodo: (todo) => {
        set((state) => ({
            todos: [ ...state.todos, todo]
        }))
    },
    completeTodo: (id) => {
        set((state) => ({
            todos: [ ...state.todos.map(todo => todo.id === id ? todo.done = true : todo)]
        }))
    },
    deleteTodo: (id) => {
        set((state) => ({
            todos: [ ...state.todos.filter(todo => todo.id !== id)]
        }))
    }
}))

export default todoState;