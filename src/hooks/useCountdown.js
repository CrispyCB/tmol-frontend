import { useEffect, useState } from "react";

const useCountdown = (targetDate) => {
    let end = new Date(targetDate);
    end.setMinutes(end.getMinutes() + 30);
    end = end.getTime();

    const [countdown, setCountdown] = useState(
        end - new Date().getTime()
    );

    useEffect(() => {
        const interval = setInterval(() => {
            setCountdown(end - new Date().getTime());
        }, 1000)

        return () => clearInterval(interval);
    }, [end]);
    return getReturnValues(countdown)
}

const getReturnValues = (countdown) => {
    const minutes = Math.floor((countdown % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((countdown % (1000 * 60)) / 1000)

    return [minutes, seconds]
}

export { useCountdown };