FROM node:16-alpine as frontend

ARG REACT_APP_SERVER=$REACT_APP_SERVER

WORKDIR /usr/app/frontend

COPY package*.json ./
RUN npm install --silent
COPY . ./
RUN npm run build

FROM nginx:latest
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
COPY --from=frontend /usr/app/frontend/build .
ENTRYPOINT ["nginx", "-g", "daemon off;"]
